/*
This file is used to initialize a fitness function. So any other function will have to have the name as "fitness" and included in
main.cpp
*/

#include<cmath>
#include<vector>
#include<algorithm>

#include "common.hpp"

float domainMin =0, domainMax =6;
std::vector<float> minDomain(dim), maxDomain(dim), minVel(dim),maxVel(dim);

void setMinDomain(){
        std::fill(std::begin(minDomain), std::end(minDomain),domainMin);
}

void setMaxDomain(){
    std::fill(std::begin(maxDomain), std::end(maxDomain), domainMax);
}

void setMinVel(){
    std::fill(std::begin(minVel), std::end(minVel), -20);
}

void setMaxVel(){
    std::fill(std::begin(maxVel), std::end(maxVel),20);
}





float fitness(std::vector<float> x){
    return sin(x[0]);
}
