#include<vector>
#include<random>
#include<chrono>
#include<algorithm>
#include<iterator>
#include<iostream>

#include "common.hpp"


template <typename t>
void printVec(std::vector<t> v){
    //To print a vector
    std::copy(std::begin(v), std::end(v),std::ostream_iterator<t>(std::cout," , "));
    std::cout<<"\n";
}


void velUpdate(std::vector<float>& v, std::vector<float> pbest, std::vector<float>gbest, std::vector<float> position){
    //This updates the velocity of a particle 
    std::uniform_real_distribution<float> unif(0,1);
    std::vector<float> subPBPres(v.size()), subGBPres(v.size()), temp(v.size()), v2(v.size());
    v2 = v;
    std::transform(std::begin(pbest), std::end(pbest),std::begin(position), std::begin(subPBPres), [](float e1, float e2){return e1-e2;});
    std::transform(std::begin(gbest),std::end(gbest), std::begin(position),std::begin(subGBPres),[](float e1, float e2){return e1-e2;});
    std::transform(std::begin(subPBPres), std::end(subPBPres),std::begin(subGBPres),std::begin(temp),[&](float p, float g){
        return (c1*unif(gen)*p) + (c2*unif(gen)*g);
    });
    std::transform(std::begin(temp),std::end(temp),std::begin(v2),std::begin(v),[](float t, float v){return v+t;});

    compareAndUpdate(minVel,v,false);
    compareAndUpdate(maxVel,v,true);
    
}