#include<vector>
#include<random>
#include<chrono>
#include<algorithm>
#include<iterator>
#include<iostream>
#include<iomanip>

#include "common.hpp"
#include"fitnessRostrign.cpp"
#include "VelUpdate.cpp"
#include "particle.cpp"
#include "comparison.cpp"


float c1=2;
float c2=c1;
constexpr int numParticles = 1000;
constexpr int numEpoch = 10000;
int dim = 2;
bool checkMaxCond = false;
std::mt19937_64 gen;
uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
std::seed_seq ss{uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed>>32)};


std::vector<float> randomizeStart(){
    
    // if(start==nullptr && std::std::end == nullptr)
    // {

    // }
    // else
    // {
    //     std::uniform_real_distribution<float> unif(start,std::end);
    // 
    std::uniform_real_distribution<float> unif(domainMin,domainMax);
    std::vector<float> p(dim);
    std::transform(std::begin(p), std::end(p),std::begin(p), [&](int el){return unif(gen);});
    return p;
}


void initialize(std::vector<particle>& p){
    for(int i=0;i<numParticles;i++)
    {
        particle par(i,randomizeStart(), velUpdate);
        p.push_back(par);
    }
}
std::vector<float> getBestVal(std::vector<particle>& particles, bool maxFlag=true){
    if(maxFlag)
    {
        auto temp = std::max_element(std::begin(particles), std::end(particles),[](particle a, particle b){
            return fitness(a.getPbest())<fitness(b.getPbest());
            });
        return temp->getPbest();
    }
    else
    {
        auto temp = std::min_element(std::begin(particles), std::end(particles),[](particle a, particle b){
            return fitness(a.getPbest())<fitness(b.getPbest());
            });
        return temp->getPbest();
    }
}

int main(){
    std::cout.setf(std::ios::fixed);
    gen.seed(ss);
    setMinDomain();
    setMaxDomain();
    setMinVel();
    setMaxVel();
    std::vector<particle> particles;
    initialize(particles);
    for(int i=0; i<numEpoch; i++)
    {
        for(auto& par:particles)
        {
            if(checkMaxCond)
                par.checkPbest(fitness,compMax);
            else
                par.checkPbest(fitness, compMin);
        }
        
       std::vector<float> val = getBestVal(particles, checkMaxCond);
        
            //std:: cout<<" The max element  id is "<<max_valuePart->getID();
        for(auto& p:particles)
        {
            p.setGbest(val);
            velUpdate(p.getVelocity(), p.getPbest(), p.getGbest(),p.getPosition());
            p.updatePosition();
        }
        if(i%1000==0)
        {
            c1= c1/2;
            c2 = c1;
            std::cout<<"For the epoch "<< i <<" fitness at best Position is"<<fitness(particles[0].getGbest())<<" whose position is ";
            printVec( particles[0].getGbest());
            std::cout<<" \t The current position is ";
            printVec(particles[0].getPosition());
            // std::cout<<"\n \t"<<" The velocity is ";
            // printVec(particles[0].getVelocity());
            std::cout<<std::endl;
        }

    }
    
    return 0;
}