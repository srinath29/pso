#include<cmath>
#include<vector>
#include<algorithm>

#include "common.hpp"


bool compMax(float pBest, float pos){
    if(pBest>pos)
        return false;
    else
        return true;
}

bool compMin(float pBest, float pos){
    if(pBest<pos)
        return false;
    else
        return true;
}


void compareAndUpdate(std::vector<float> base, std::vector<float>& v, bool maxFlag){
    //based on the max flag it will update the velocity which is passed by ref
    std::vector<float> temp(base.size());
    std::transform(std::begin(base), std::end(base),std::begin(v),std::begin(temp),[&](float bas, float vel){
        if(maxFlag)
        {
            if(vel>bas)
                return bas;
            else
                return vel;
        }
        else
        {
            if(vel<bas)
                return bas;
            else
                return vel;
        }
    });
    v = temp;
}