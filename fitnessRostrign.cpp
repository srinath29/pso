/*
This file is used to initialize a fitness function. So any other function will have to have the name as "fitness" and included in
main.cpp
*/

#include<cmath>
#include<vector>
#include<algorithm>

#include "common.hpp"

float domainMax =5.12, domainMin = -5.12;
std::vector<float> minDomain(dim), maxDomain(dim), minVel(dim),maxVel(dim);

void setMinDomain(){
        std::fill(std::begin(minDomain), std::end(minDomain),domainMin);
}

void setMaxDomain(){
    std::fill(std::begin(maxDomain), std::end(maxDomain), domainMax);
}

void setMinVel(){
    std::fill(std::begin(minVel), std::end(minVel), -20);
}

void setMaxVel(){
    std::fill(std::begin(maxVel), std::end(maxVel),20);
}

float fitness(std::vector<float> t){
    float PI = atan(1)*4;
    float A =10;
    int n = dim;
    float sum = std::accumulate(std::begin(t), std::end(t),0,[&](float a, float b){
       auto temp = pow(b,2)-A*cos(2*PI*b);
       return a+temp;
    });
    return sum+A*n;
}