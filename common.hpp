/*
This header is to include some common properties and to be shared between multiple source files
*/

#include<vector>
#include<random>


extern float c1,c2;
extern float minX,maxX;
extern std::vector<float> minDomain, maxDomain, minVel, maxVel;
extern int dim;
extern std::mt19937_64 gen;
extern float domainMin, domainMax;
template<typename t>
void printVec(std::vector<t> l);

void compareAndUpdate(std::vector<float> base, std::vector<float>& v, bool maxFlag);