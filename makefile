CC = g++
TARGET = pso
FLAGS = --std==c++11

all : main.cpp comparison.cpp fitness.cpp particle.cpp VelUpdate.cpp
    $(CC) $(FLAGS) main.cpp -o $(TARGET)
