# README #

This project's goal is to implement PSO (particle swarm optimisation) using c++ STL library. 


### How do I get set up? ###

* clone the repo
* make sure your compiler has c++11 support



### Who do I talk to? ###

* Repo owner


###To Compile###
```` 
g++ -std=c++11 main.cpp
````

###Implementation###
This code is implemented using [this](http://www.scholarpedia.org/article/Particle_swarm_optimization) theory

