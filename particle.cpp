#include<vector>
#include<iterator>
#include<algorithm>
#include<iostream>
#include<functional>

#include"common.hpp"

class particle{

    

    private:
        std::vector<float> velocity;
        std::vector<float> position;
        std::vector<float> pbest;
        std::vector<float> gbest;
        bool initialized = false ;
        int particleID;

    public:
        particle(int pID,std::vector<float> pos, std::function<void( std::vector<float>&, std::vector<float>, std::vector<float>, std::vector<float> )> updateVel):particleID(pID){
        std::vector<float> temp(pos.size());
        position = pos;
        pbest = pos;
        gbest = temp;
        velocity = temp;
        //updateVel(velocity, pbest, gbest, position);
        initialized = true;
        //updatePosition();
    }
        std::vector<float>& getVelocity(){
            return velocity;
        }
        std::vector<float> getPosition(){
            return position;
        }
        int getID(){
            return particleID;
        }
        std::vector<float> getPbest(){
            return pbest;
        }
        std::vector<float> getGbest(){
            return gbest;
        }

        void setVelocity(std::vector<float> v){
            velocity = v;
        }
        void setPosition(std::vector<float> p){
            position = p;
        }
        void setGbest(std::vector<float> g){
            gbest =g;
        }
        void checkPbest(std::function<float(std::vector<float>)> cost, std::function<bool(float,float)> comp){
            if(initialized)
            {
                // std::cout<<"For particle "<<particleID;
                // std::cout<<"The pbest is ";
                // printVec(pbest);
                // std::cout<<" The position is ";
                // printVec(position);
                // std::cout<< " The comparison is "<< comp(cost(pbest),cost(position))<<" for cost value of pbest"<<cost(pbest)<<" and cost value of position "<<cost(position)<<"\n";

                if( comp(cost(pbest),cost(position)))
                {
                    pbest = position;
                }
                
            }
            else
            {
                std::cout<<"This particle has not been initialized"<<std::endl;
            }
        }
        void updatePosition(){
            std::vector<float>temp(position);
            std::transform(std::begin(temp), std::end(temp), std::begin(velocity), std::begin(position),[](float e1, float e2){return e1+e2;});
            // std::replace_if(std::begin(position), std::end(position),[](float x){return x>maxX;}, maxX);
            // std::replace_if(std::begin(position), std::end(position),[](float x){return x<minX;}, minX);
            compareAndUpdate(minDomain,position, false );
            compareAndUpdate(maxDomain, position, true);
        }
};